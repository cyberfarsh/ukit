from django.urls import path, re_path, include
from . import views
from api.views import auth_check

urlpatterns = [
    # path('api/v1/map/', MapList.as_view()),
    # path('api/v1/moscow/', MoscowList.as_view()),
    # path('api/v1/point/', PointList.as_view()),
    re_path(r'.*', views.index),
]