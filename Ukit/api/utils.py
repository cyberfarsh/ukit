from rest_framework.exceptions import status
from rest_framework.views import exception_handler
from django.http import JsonResponse


def custom_exception_handler(exc, context):
    response = exception_handler(exc, context)
    if response is not None:
        return JsonResponse(response.data, status=response.status_code)
    return JsonResponse({"detail": f"{exc.__class__.__name__}: {exc}"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
